import pygame
from projectile import Projectile

#Creation class joueur 
class Player(pygame.sprite.Sprite):

    def __init__(self, game):
        super().__init__()
        self.game = game
        self.health = 100
        self.max_health = 100
        self.attack = 10 
        self.velocity = 5
        self.all_projectile = pygame.sprite.Group()
        self.image = pygame.image.load('assets/ling.png')
        self.rect = self.image.get_rect()
        self.rect.x = 400
        self.rect.y = 450


    def damage(self, amount):
        if self.health - amount > amount:
            self.health -= amount
        else: 
            self.game.game_over()


    def update_health_bar(self, surface):
        
        pygame.draw.rect(surface, (60, 63, 60), [self.rect.x + 70, self.rect.y -20, self.max_health, 8])
        pygame.draw.rect(surface, (111, 210, 46), [self.rect.x + 70, self.rect.y -20, self.health, 8])


    def launch_projectile(self):
        self.all_projectile.add(Projectile(self))


    def move_right(self):
        #collision 
        if not self.game.check_collision(self, self.game.all_monsters):
            self.rect.x += self.velocity

    def move_left(self):
            self.rect.x -= self.velocity
        

