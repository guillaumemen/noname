# :video_game: Noname :video_game:
 
 
 ## Description du projet
 
 Noname est un jeu de type arcade 2D :punch:
 
 ## Technologie 
 
 Ce jeu est developpé en Python avec la librairie Pygame 
 
 Pygame est une librairie crée en 2000 permetant l'affichage video 2D mais aussi l'audio et la gestion des touches 
 
 ## On teste ? 
 
 
     - Clone du projet: 
         Git clone https://gitlab.com/guillaumemen/noname
     
     - Installer Pygame 
         [Pygame](https://www.pygame.org/wiki/GettingStarted)
     
     - Ouvrir le projet 
         Cd noname
     
     - Exécutez 
         Python3 main.py
     
     - On lance 
         Un clique sur play suffit 
         
 ## Les touches 
 
 Les commandes ne sont pas compliqué les fleches pour droite gauche et espace pour les attaques :punch: