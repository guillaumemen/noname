import pygame 
from monster import Monster
from player import Player

#Seconde class jeu 
class Game:
    def __init__(self):
        #commncer
        self.is_playing = False
        #genrer le player
        self.all_players = pygame.sprite.Group()
        self.player = Player(self)
        self.all_players.add(self.player)
        #groupe monstre
        self.all_monsters = pygame.sprite.Group()
        self.pressed = {}
        

    def start(self):
            self.is_playing = True
            self.spawn_monster()
            self.spawn_monster()

    def game_over(self):
            self.all_monsters = pygame.sprite.Group()
            self.player.health = self.player.max_health
            self.is_playing = False


    def update(self, screen):
             #Player
        screen.blit(self.player.image, self.player.rect)
    
            #barre de vie 
        self.player.update_health_bar(screen)

            #recuperer projectile
        for projectile in self.player.all_projectile:
                projectile.move()

             # Recuperer Monstres 
        for monster in self.all_monsters:
                monster.forward()
                monster.update_health_bar(screen)
        #projectile
        self.player.all_projectile.draw(screen)
            
        #aplliquermosntre 
        self.all_monsters.draw(screen)
            
        #Droite ou Gauche 
        if self.pressed.get(pygame.K_RIGHT) and self.player.rect.x + self.player.rect.width < screen.get_width():
            self.player.move_right()
        elif self.pressed.get(pygame.K_LEFT) and self.player.rect.x > -20:
            self.player.move_left()


    

    def check_collision(self, sprite, group):
        return pygame.sprite.spritecollide(sprite, group, False, pygame.sprite.collide_mask)

    def spawn_monster(self):
        monster = Monster(self)
        self.all_monsters.add(monster)
